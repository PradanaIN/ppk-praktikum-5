# PPK-Praktikum 5 : JSON Web Token (JWT)

## Identitas

```
Nama : Novanni Indi Pradana
NIM  : 222011436
Kelas: 3SI3

```

## Deskripsi

Seperti namanya, JSON Web Token, yang berarti token ini menggunakan JSON (Javascript Object Notation), lalu token ini memungkinkan kita untuk mengirimkan data yang dapat diverifikasi oleh dua pihak atau lebih. 


## Kegiatan Praktikum

## 1. Register
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-5/-/raw/main/screenshot/Screenshot%20(1).png)
## 2. Login
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-5/-/raw/main/screenshot/Screenshot%20(2).png)
## 3. Get Current User Login
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-5/-/raw/main/screenshot/Screenshot%20(3).png)
## 3. Access Protected Resources
### 200 OK
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-5/-/raw/main/screenshot/Screenshot%20(4).png)
### 401 Unauthorized : "Token Required"
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-5/-/raw/main/screenshot/Screenshot%20(5).png)
### 401 Unauthorized : "Invalid Token"
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-5/-/raw/main/screenshot/Screenshot%20(6).png)
